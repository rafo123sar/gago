<?php declare(strict_types=1);

namespace SpsTheme\Extension;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ProductMediaExtension extends AbstractExtension
{
    /**
     * @var EntityRepositoryInterface
     */
    private $productMediaRepository;
    public function __construct(EntityRepositoryInterface $productMediaRepository)
    {
        $this->productMediaRepository = $productMediaRepository;
    }
    public function getFunctions(): array
    {
        return [
            new TwigFunction('searchProductsMedia', [$this, 'searchProductsMedia']),
        ];
    }
    public function searchProductsMedia(Context $context)
    {
        $criteria = new Criteria();
        /** @var ProductExtension $productMedia */
        $productMedia = $this->productMediaRepository
            ->search($criteria, $context)
            ->getEntities();
        return $productMedia;
    }
}
