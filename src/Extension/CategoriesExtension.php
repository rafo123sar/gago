<?php declare(strict_types=1);

namespace SpsTheme\Extension;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CategoriesExtension extends AbstractExtension
{
    /**
     * @var EntityRepositoryInterface
     */
    private $categoryRepository;

    public function __construct(EntityRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('searchCategory', [$this, 'searchCategory']),
        ];
    }

    public function searchCategory(Context $context)
    {
        $criteria = new Criteria();
        /** @var CategoriesExtension $categories */
        $categories = $this->categoryRepository
            ->search($criteria, $context)
            ->getEntities();
        return $categories;
    }
}
