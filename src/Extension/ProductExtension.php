<?php declare(strict_types=1);

namespace SpsTheme\Extension;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ProductExtension extends AbstractExtension
{
    /**
     * @var EntityRepositoryInterface
     */
    private $productRepository;
    public function __construct(EntityRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }
    public function getFunctions(): array
    {
        return [
            new TwigFunction('searchProducts', [$this, 'searchProducts']),
        ];
    }
    public function searchProducts(Context $context)
    {
        $criteria = new Criteria();
        /** @var ProductExtension $products */
        $products = $this->productRepository
            ->search($criteria, $context)
            ->getEntities();
        return $products;
    }
}
