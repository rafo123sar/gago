<?php declare(strict_types=1);

namespace SpsTheme;

use Shopware\Core\Framework\Plugin;
use SpsTheme\Util\Lifecycle\LifecycleManager;
use SpsTheme\Manager\CustomFieldManager;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Plugin\Context\ActivateContext;
use Shopware\Core\Framework\Plugin\Context\DeactivateContext;
use Shopware\Core\Framework\Plugin\Context\InstallContext;
use Shopware\Core\Framework\Plugin\Context\UninstallContext;
use Shopware\Core\Framework\Plugin\Context\UpdateContext;
use Shopware\Storefront\Framework\ThemeInterface;


class SpsTheme extends Plugin implements ThemeInterface
{
    public function install(InstallContext $context): void
    {
        $lifecycleManager = new LifecycleManager($this, $this->container);
        $lifecycleManager->Install($context);
        parent::install($context);
        ($this->getCustomFieldManager($context->getContext()))->install();
    }
    /**
     * @param Context $context
     * @return CustomFieldManager
     */
    private function getCustomFieldManager(Context $context): CustomFieldManager
    {
        return new CustomFieldManager(
            $context,
            $this->container->get('custom_field_set.repository'),
            $this->container->get('snippet.repository')
        );
    }

    public function Update(UpdateContext $context): void
    {
        parent::Update($context);
        ($this->getCustomFieldManager($context->getContext()))->install();
    }

    public function uninstall(UninstallContext $context): void
    {
        $lifecycleManager = new LifecycleManager($this, $this->container);
        $lifecycleManager->Uninstall($context);
        parent::uninstall($context);
        ($this->getCustomFieldManager($context->getContext()))->uninstall();
    }
    public function activate(ActivateContext $context): void
    {
        $lifecycleManager = new LifecycleManager($this, $this->container);
        $lifecycleManager->Activate($context);
        parent::activate($context);
    }
    public function deactivate(DeactivateContext $context): void
    {
        $lifecycleManager = new LifecycleManager($this, $this->container);
        $lifecycleManager->Deactivate($context);
        parent::deactivate($context);
    }

    public function getThemeConfigPath(): string
    {
        return 'theme.json';
    }
}