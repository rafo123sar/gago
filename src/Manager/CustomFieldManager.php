<?php declare(strict_types=1);

namespace SpsTheme\Manager;

use Exception;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\ContainsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\IdSearchResult;
use Shopware\Core\System\CustomField\CustomFieldTypes;

/**
 * Class CustomFieldManager
 * @package CustomFieldManager\Storefront\Manager
 */
class CustomFieldManager
{
    /**
     * @param string PREFIX
     */
    public const PREFIX = 'prodCustomDesc';

    /**
     * @param array SETS
     */
    public const SETS = [

        [
            'name' => self::PREFIX . '_prod_custom_data',
            'active' => true,
            'config' => [
                'label' => [
                    'de-DE' => 'Produkt Custom Daten',
                    'en-GB' => 'Product Custom Data'
                ]
            ],
            'customFields' => [
                 [
                    'name' => self::PREFIX . '_description',
                    'type' => CustomFieldTypes::HTML,
                    'config' => [
                        'componentName' => 'sw-text-editor',
                        'customFieldType' => 'textEditor',
                        'label' => [
                            'de-DE' => 'Custom attributes',
                            'en-GB' => 'Custom attributes',
                        ],
                        'customFieldPosition' => 2,
                        'disabled' => false
                    ],

                ]
            ],
            'relations' => [
                [
                    'entityName' => 'product',
                ],
            ]
        ],
        [
            'name' => self::PREFIX . '_product_custom_characteristic',
            'active' => true,
            'config' => [
                'label' => [
                    'de-DE' => 'Produktbenutzerdefinierte Eigenschaft',
                    'en-GB' => 'Product Custom Characteristic'
                ]
            ],
            'customFields' => [
                [
                    'name' => self::PREFIX . '_characteristic_1',
                    'type' => CustomFieldTypes::TEXT,
                    'config' => [
                        'componentName' => 'sw-text-field',
                        'customFieldType' => 'text',
                        'label' => [
                            'de-DE' => 'field 1',
                            'en-GB' => 'field 1 ',
                        ],
                        'customFieldPosition' => 1,
                        'disabled' => false
                    ]
                ],
                [
                    'name' => self::PREFIX . '_characteristic_2',
                    'type' => CustomFieldTypes::TEXT,
                    'config' => [
                        'componentName' => 'sw-text-field',
                        'customFieldType' => 'text',
                        'label' => [
                            'de-DE' => 'field 2',
                            'en-GB' => 'field 2 ',
                        ],
                        'customFieldPosition' => 2,
                        'disabled' => false
                    ]
                ],
                [
                    'name' => self::PREFIX . '_characteristic_3',
                    'type' => CustomFieldTypes::TEXT,
                    'config' => [
                        'componentName' => 'sw-text-field',
                        'customFieldType' => 'text',
                        'label' => [
                            'de-DE' => 'field 3',
                            'en-GB' => 'field 3',
                        ],
                        'customFieldPosition' => 3,
                        'disabled' => false
                    ]
                ]
            ],
            'relations' => [
                [
                    'entityName' => 'product',
                ],
            ]
        ]
    ];

    /**
     * @var Context
     */
    private $context;

    /**
     * @var EntityRepositoryInterface
     */
    private $customFieldSetRepository;

    /**
     * @var EntityRepositoryInterface
     */
    private $snippetRepository;

    /**
     * CustomFieldManager constructor.
     * @param Context $context
     * @param EntityRepositoryInterface $customFieldSetRepository
     */
    public function __construct(
        Context $context,
        EntityRepositoryInterface $customFieldSetRepository,
        EntityRepositoryInterface $snippetRepository
    )
    {
        $this->context = $context;
        $this->customFieldSetRepository = $customFieldSetRepository;
        $this->snippetRepository = $snippetRepository;
    }


    /**
     *
     */
    public function install(): void
    {
        foreach (self::SETS as $SET) {
            try {
                $this->customFieldSetRepository->upsert([$SET], $this->context);
            } catch (Exception $exception) {

            }
        }
    }

    public function uninstall(): void
    {
        $this->removeCustomFields();
        $this->removeSnippets();
    }

    private function removeCustomFields()
    {
        $customFieldIds = $this->getCustomFieldSetIds();

        if ($customFieldIds->getTotal() === 0) {
            return;
        }

        $ids = array_map(static function ($id) {
            return ['id' => $id];
        }, $customFieldIds->getIds());

        $this->customFieldSetRepository->delete($ids, $this->context);
    }

    /**
     * @return IdSearchResult
     */
    private function getCustomFieldSetIds(): IdSearchResult
    {
        $criteria = new Criteria();
        $criteria->addFilter(new ContainsFilter('name', self::PREFIX));

        return $this->customFieldSetRepository->searchIds($criteria, $this->context);
    }

    private function removeSnippets()
    {
        $snippetIds = $this->getSnippetIds();

        if ($snippetIds->getTotal() === 0) {
            return;
        }

        $ids = array_map(static function ($id) {
            return ['id' => $id];
        }, $snippetIds->getIds());

        $this->snippetRepository->delete($ids, $this->context);
    }

    /**
     * @return IdSearchResult
     */
    private function getSnippetIds(): IdSearchResult
    {
        $criteria = new Criteria();
        $criteria->addFilter(new ContainsFilter('translationKey', self::PREFIX));

        return $this->snippetRepository->searchIds($criteria, $this->context);
    }
}
