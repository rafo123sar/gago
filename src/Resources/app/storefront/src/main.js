import searchToggle from "./scripts/searchToggle.plugin";
import ListingViewMode from './scripts/listing-view-mode';
import OverrideOffcanvasMenuPlugin from "./scripts/overrideOffcanvasMenu.plugin";

const PluginManager = window.PluginManager;
PluginManager.register('searchToggle', searchToggle, '.main-navigation-menu');
PluginManager.register('ListingViewMode', ListingViewMode, '[data-view-mode]');
PluginManager.override('OffcanvasMenu', OverrideOffcanvasMenuPlugin, '[data-offcanvas-menu]');