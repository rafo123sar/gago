import Plugin from 'src/plugin-system/plugin.class';
import DomAccess from 'src/helper/dom-access.helper';

export default class ListingViewMode extends Plugin {

    /**
     * Plugin options
     * @type {{gridView: String, listView: String, wrapperSelector: String, activeCls: String}}
     */
    static options = {
        modeChangeButtons: '.listing-view-button',
        wrapperSelector: '.js-listing-wrapper',
        activeCls: 'is-active'
    }

    /**
     * Plugin initializer
     *
     * @returns {void}
     */
    init() {
        this.modeChangeButtons = DomAccess.querySelectorAll(this.el, this.options.modeChangeButtons);
        this.listingWrapper = document.querySelector(this.options.wrapperSelector);
        this.activeCls = this.options.activeCls;

        this.registerEvents();
    }

    registerEvents() {
        var me = this;

        this.modeChangeButtons.forEach((button) => {
            button.addEventListener('click', this.changeViewMode.bind(this, button));
        });

        me.setViewMode();

    }

    setViewMode() {
        if (localStorage.getItem('ProductListingView')) {
            document.querySelector('.js-listing-wrapper').setAttribute('data-view', localStorage.getItem('ProductListingView'));
            this.setActiveClass();
        } else {
            localStorage.setItem('ProductListingView', 'list');
            DomAccess.querySelector(this.el, '.listing-view-button.listing-view-type-list').classList.add(this.options.activeCls);
        }
    }

    setActiveClass() {
        this.modeChangeButtons.forEach((button) => {
            if (button.getAttribute('data-view') === localStorage.getItem('ProductListingView')) {
                button.classList.add(this.activeCls);
            } else {
                button.classList.remove(this.activeCls);
            }
        });
    }

    changeViewMode(button) {
        localStorage.setItem('ProductListingView', button.getAttribute('data-view'));
        this.setViewMode();
    }
}
