import Plugin from "src/plugin-system/plugin.class";
import DomAccess from "src/helper/dom-access.helper";

export default class searchToggle extends Plugin {
    init() {

        this.body = document.querySelector('body');

        this.searchButton = DomAccess.querySelector(this.el, '.search-toggle-btn');
        this.searchBlock = DomAccess.querySelector(this.el, '.search-block');
        this.searchActiveCls = 'search-active';

        this.registerEvents();
    }

    registerEvents() {
        this.searchButton.addEventListener('click', this._openSearch.bind(this), false);
        this.body.addEventListener('click', this._closeSearch.bind(this), false);
        this.searchBlock.addEventListener('click', this._stopPropagation.bind(this), false);

    }

    _openSearch(event) {
        this.searchBlock.classList.add(this.searchActiveCls);
        this.searchButton.classList.add(this.searchActiveCls);
        event.stopPropagation();
    }

    _closeSearch() {
        this.searchBlock.classList.remove(this.searchActiveCls);
        this.searchButton.classList.remove(this.searchActiveCls);
    }

    _stopPropagation(event){
        event.stopPropagation();
    }
}