import OffcanvasMenuPlugin from 'src/plugin/main-menu/offcanvas-menu.plugin';

export default class OverrideOffcanvasMenuPlugin extends OffcanvasMenuPlugin {

    static options = {
        navigationUrl: window.router['frontend.menu.offcanvas'],
        position: 'right',
        tiggerEvent: 'click',

        additionalOffcanvasClass: 'navigation-offcanvas',
        linkSelector: '.js-navigation-offcanvas-link',
        loadingIconSelector: '.js-navigation-offcanvas-loading-icon',
        linkLoadingClass: 'is-loading',
        menuSelector: '.js-navigation-offcanvas',
        overlayContentSelector: '.js-navigation-offcanvas-overlay-content',
        initialContentSelector: '.js-navigation-offcanvas-initial-content',

        homeBtnClass: 'is-home-link',
        backBtnClass: 'is-back-link',
        transitionClass: 'has-transition',
        overlayClass: '.navigation-offcanvas-overlay',
        placeholderClass: '.navigation-offcanvas-placeholder',

        forwardAnimationType: 'forwards',
        backwardAnimationType: 'backwards',
    };
}