import './module/sw-cms/blocks/text/quick-facts';
import './module/sw-cms/blocks/text/breadcrumb-block';
import './module/sw-cms/blocks/text/text-bullets-block';
import './module/sw-cms/blocks/text-image/banner-text-block';
import './module/sw-cms/blocks/text-image/services-block';
import './module/sw-cms/blocks/text-image/benefits-block';
import './module/sw-cms/blocks/text-image/two-boxes';
import './module/sw-cms/blocks/form/newsletter-block';
import './module/sw-cms/blocks/commerce/product-grid-block';
import './module/sw-cms/blocks/text-image/content-slider-block';
import './module/sw-cms/blocks/text-image/dual-teaser';
import './module/sw-cms/blocks/text-image/dual-text-image-block';
import './module/sw-cms/blocks/text-image/seven-steps-block';
import './module/sw-cms/blocks/image/sps-slider-block';
import './module/sw-cms/blocks/text/info-circles-block';
import './module/sw-cms/blocks/text/dialog';
import './module/sw-cms/blocks/text-image/subcategory-teaser-block';
import './module/sw-cms/blocks/text-image/third-grid';

import './module/sw-cms/elements/quick-facts';
import './module/sw-cms/elements/breadcrumb';
import './module/sw-cms/elements/text-bullets';
import './module/sw-cms/elements/banner-text';
import './module/sw-cms/elements/services';
import './module/sw-cms/elements/benefits';
import './module/sw-cms/elements/newsletter';
import './module/sw-cms/elements/product-grid';
import './module/sw-cms/elements/content-slider';
import './module/sw-cms/elements/dual-teaser';
import './module/sw-cms/elements/two-boxes';
import './module/sw-cms/elements/dual-text-image';
import './module/sw-cms/elements/seven-steps';
import './module/sw-cms/elements/sps-slider';
import './module/sw-cms/elements/info-circles';
import './module/sw-cms/elements/dialog';
import './module/sw-cms/elements/subcategory-teaser';
import './module/sw-cms/elements/third-grid';


