import template from './sw-cms-el-preview-content-slider.html.twig';
import './sw-cms-el-preview-content-slider.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-content-slider', {
    template
});
