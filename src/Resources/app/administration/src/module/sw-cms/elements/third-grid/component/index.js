import template from './sw-cms-el-third-grid.html.twig';
import './sw-cms-el-third-grid.scss';

const { Component, Mixin, Filter } = Shopware;

Component.register('sw-cms-el-third-grid', {
    template,

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    computed: {

        assetFilter() {
            return Filter.getByName('asset');
        },

        mediaConfigValue() {
            return this.element?.config?.sliderItems?.value;
        },
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('third-grid');
            this.initElementData('third-grid');
        },
    },
});
