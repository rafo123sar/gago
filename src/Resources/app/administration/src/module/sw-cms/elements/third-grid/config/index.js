import template from './sw-cms-el-config-third-grid.html.twig';
import './sw-cms-el-config-third-grid.scss';

const { Component, Mixin } = Shopware;

Component.register('sw-cms-el-config-third-grid', {
    template,

    inject: ['repositoryFactory'],

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    data() {
        return {
            mediaModalIsOpen: false,
            initialFolderId: null,
        };
    },

    computed: {
        mediaRepository() {
            return this.repositoryFactory.create('media');
        },
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('third-grid');
        },

        uploadTag(index) {
            return `cms-element-media-config-${this.element.id}-${index}`;
        },

        previewSource(index) {
            if (this.element.data && this.element.data[index] && this.element.data[index].media && this.element.data[index].media.id) {
                return this.element.data[index].media;
            }
            return this.element.config.medias.value[index].media.value;
        },

        async onImageUpload({ targetId }, index) {
            const mediaEntity = await this.mediaRepository.get(targetId);

            this.element.config.medias.value[index].media.value = mediaEntity.id;

            this.updateElementData(index, mediaEntity);

            this.$emit('element-update', this.element);
        },

        onImageRemove(index) {
            this.element.config.medias.value[index].media.value = null;

            this.updateElementData(index);

            this.$emit('element-update', this.element);
        },
        
        onSelectionChanges(mediaEntity, index) {
            const media = mediaEntity[0];
            this.element.config.medias.value[index].media.value = media.id;

            this.updateElementData(index, media);

            this.$emit('element-update', this.element);
        },

        updateElementData(index, media = null) {
            const mediaId = media === null ? null : media.id;

            if (!this.element.data) {
                this.$set(this.element, 'data', { mediaId });
                this.$set(this.element, 'data', { media });
            } else {
                this.element.config.medias.value[index].media.value = media;
            }
        },
    },
});
