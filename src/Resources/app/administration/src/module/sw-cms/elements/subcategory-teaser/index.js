import './component';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
  name: 'subcategory-teaser',
  label: 'Subcategories Teasers',
  component: 'sw-cms-el-subcategory-teaser',
  configComponent: 'sw-cms-el-config-subcategory-teaser',
  previewComponent: 'sw-cms-el-preview-subcategory-teaser'
});
