import template from './sw-cms-el-preview-product-grid.html.twig';
import './sw-cms-el-preview-product-grid.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-product-grid', {
    template
});
