import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'quick-facts',
    label: 'quick-facts',
    component: 'sw-cms-el-quick-facts',
    configComponent: 'sw-cms-el-config-quick-facts',
    previewComponent: 'sw-cms-el-preview-quick-facts',
    defaultConfig: {
        headline: {
            source: 'static',
            value: 'QUICK FACTS',
        },
        content: {
            source: 'static',
            value: `
                <p>Lorem Ipsum dolor sit amet</p>
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, 
                sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, 
                sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. 
                Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. 
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, 
                sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. 
                At vero eos et accusam et justo duo dolores et ea rebum. 
                Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
            `.trim(),
        },
        buttonTitle:{
            source: 'static',
            value: 'Weiter',
        },
        url: {
            source: 'static',
            value: '',
        },
        newTab: {
            source: 'static',
            value: false,
        }
    },
});
