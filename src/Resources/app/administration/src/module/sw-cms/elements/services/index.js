import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'services',
    label: 'Services',
    component: 'sw-cms-el-services',
    configComponent: 'sw-cms-el-config-services',
    previewComponent: 'sw-cms-el-preview-services',
    defaultConfig: {
        header:{
            source: 'static',
            value: null
        },
        supText:{
            source: 'static',
            value: null
        },
        textBlock:{
            source: 'static',
            value: null
        },
        sliders: {
            source: 'static',
            value: [{
                media: {
                    source: 'static',
                    value: null,
                    required: true,
                    entity: {
                        name: 'media'
                    }
                },
                headline: {
                    source: 'static',
                    value: null
                },

                url: {
                    source: 'static',
                    value: null
                }
            }]
        }
    }
});
