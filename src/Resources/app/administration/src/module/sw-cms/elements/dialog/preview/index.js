import template from './sw-cms-el-preview-dialog.html.twig';
import './sw-cms-el-preview-dialog.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-dialog', {
    template,
});
