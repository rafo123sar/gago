import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'dual-text-image',
    label: 'Text block with wide image',
    component: 'sw-cms-el-dual-text-image',
    configComponent: 'sw-cms-el-config-dual-text-image',
    previewComponent: 'sw-cms-el-preview-dual-text-image',
    defaultConfig: {
        media: {
            source: 'static',
            value: null,
            required: true,
            entity: {
                name: 'media'
            }
        },
        headline: {
            source: 'static',
            value: null
        },

        subLine: {
            source: 'static',
            value: null
        },
        description: {
            source: 'static',
            value: null
        },
        buttonText:{
            source: 'static',
            value: '',
        },
        url: {
            source: 'static',
            value: '',
        }
    }
});
