import template from './sw-cms-el-dual-teaser.html.twig';
import './sw-cms-el-dual-teaser.scss';

const { Component, Mixin } = Shopware;

Component.register('sw-cms-el-dual-teaser', {
  template,

  mixins: [
    Mixin.getByName('cms-element')
  ],

  created() {
    this.createdComponent();
  },

  methods: {
    createdComponent() {
      this.initElementConfig('dual-teaser');
      this.initElementData('dual-teaser');
    }
  }
});