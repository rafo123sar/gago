import template from './sw-cms-el-subcategory-teaser.html.twig';
import './sw-cms-el-subcategory-teaser.scss';

const { Component, Mixin } = Shopware;

Component.register('sw-cms-el-subcategory-teaser', {
  template,

  mixins: [
    Mixin.getByName('cms-element')
  ],

  created() {
    this.createdComponent();
  },

  methods: {
    createdComponent() {
      this.initElementConfig('subcategory-teaser');
      this.initElementData('subcategory-teaser');
    }
  }
});