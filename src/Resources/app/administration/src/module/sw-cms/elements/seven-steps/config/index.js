import template from './sw-cms-el-config-seven-steps.html.twig';
import './sw-cms-el-config-seven-steps.scss';

const { Component, Mixin } = Shopware;

Component.register('sw-cms-el-config-seven-steps', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    inject: ['repositoryFactory'],

    data() {
        return {
            mediaModalIsOpen: false,
            mediaModalIsOpen2: false,
            initialFolderId: null,
            mediaModalIndex: null,
            mediaModalIndex2: null,
            configModalIndex: null,
            currentModalConfiguration: null,
            currentModalData: null,
            showModal: false
        };
    },

    computed: {
        mediaRepository() {
            return this.repositoryFactory.create('media');
        }
    },

    created() {
        this.createdComponent();
    },

    methods: {
        uploadTag(index) {
            return `cms-element-media-config-${this.element.id}-${index}`;
        },
        uploadTag2() {
            return `cms-element-media-config-${this.element.id}`;
        },


        previewSource(index) {
            // if (this.element.data && this.element.data.media && this.element.data.media.id) {
            //     return this.element.data.media;
            // }
            //
            // return this.element.config.media.value;
            if (this.element.data && this.element.data[index] && this.element.data[index].mediaId) {
                return this.element.data[index].mediaId;
            }

            return this.element.config.sliders.value[index].media.value;
        },

        previewSource2() {
            if (this.element.data && this.element.data.media && this.element.data.media.id) {
                return this.element.data.media;
            }

            return this.element.config.media2.value;
        },

        addElement() {
            this.element.config.sliders.value.push({
                media: {
                    source: 'static',
                    value: null,
                    required: true,
                    entity: {
                        name: 'media'
                    }
                },
                headline: {
                    source: 'static',
                    value: null
                },
                description: {
                    source: 'static',
                    value: null
                }
            });
            this.$emit('element-update', this.element);
        },

        removeElement(index) {
            if ((index === 0 || index) &&
                this.element.config.sliders.value &&
                this.element.config.sliders.value[index]
            ) {
                this.element.config.sliders.value.splice(index, 1);
            }
        },

        createdComponent() {
            this.initElementConfig('seven-steps');
        },

        onImageUpload(index, target) {
            this.mediaRepository.get(target.targetId, Shopware.Context.api).then((mediaEntity) => {
                if ((index === 0 || index) &&
                    this.element.config &&
                    this.element.config.sliders.value &&
                    this.element.config.sliders.value[index] &&
                    this.element.config.sliders.value[index].media
                ) {
                    this.element.config.sliders.value[index].media.value = mediaEntity ? mediaEntity.id : null;
                }
                this.updateElementData({'media': mediaEntity, 'index': index});
                this.$emit('element-update', this.element);
            });
        },

        async onImageUpload2({ targetId }) {
            const mediaEntity = await this.mediaRepository.get(targetId, Shopware.Context.api);

            this.element.config.media2.value = mediaEntity.id;

            this.updateElementData2(mediaEntity);

            this.$emit('element-update', this.element);
        },

        onImageRemove(index) {
            index = (index === 0 || index) ? index : this.mediaModalIndex;
            if (this.element.config &&
                this.element.config.sliders.value &&
                this.element.config.sliders.value[index] &&
                this.element.config.sliders.value[index].media
            ) {
                this.element.config.sliders.value[index].media.value = null;
            }

            this.updateElementData({'media': null, 'index': index});

            this.$emit('element-update', this.element);
        },

        onImageRemove2() {
            this.element.config.media2.value = null;

            this.updateElementData2();

            this.$emit('element-update', this.element);
        },

        onCloseModal() {
            this.mediaModalIsOpen = false;
            this.mediaModalIndex = null;
        },
        onCloseModal2() {
            this.mediaModalIsOpen2 = false;
            this.mediaModalIndex2 = null;
        },



        onSelectionChanges(mediaEntity) {
            const media = mediaEntity[0];

            if ((this.mediaModalIndex || this.mediaModalIndex == 0) &&
                this.element.config &&
                this.element.config.sliders.value &&
                this.element.config.sliders.value[this.mediaModalIndex] &&
                this.element.config.sliders.value[this.mediaModalIndex].media
            ) {
                this.element.config.sliders.value[this.mediaModalIndex].media.value = media ? media.id : null;
                this.element.config.sliders.value[this.mediaModalIndex].media.media = media ? media : null;
            }

            this.updateElementData({'media': media, 'index': this.mediaModalIndex});
            this.$emit('element-update', this.element);
        },
        onSelectionChanges2(mediaEntity) {
            const media = mediaEntity[0];
            this.element.config.media2.value = media.id;

            this.updateElementData2(media);

            this.$emit('element-update', this.element);
        },

        updateElementData(data) {
            if (data.index || data.index == 0) {
                this.element.config.sliders.value[data.index].media.value = data.media ? data.media.id : null;
                this.element.config.sliders.value[data.index].media.media = data.media ? data.media : null;
                this.$set(this.element.data, data.index, {
                    'media': data.media,
                    'mediaId': data.media ? data.media.id : null
                });
            }
        },

        updateElementData2(media = null) {
            const mediaId = media === null ? null : media.id;

            if (!this.element.data) {
                this.$set(this.element, 'data', { mediaId });
                this.$set(this.element, 'data', { media });
            } else {
                this.$set(this.element.data, 'mediaId', mediaId);
                this.$set(this.element.data, 'media', media);
            }
        },

        onOpenMediaModal(index) {
           this.mediaModalIndex = index;
           this.mediaModalIsOpen = true;
        },

        onOpenMediaModal2() {
            this.mediaModalIsOpen2 = true;
        },

        onChangeMinHeight(value) {
            this.$emit('element-update', this.element);
        }
    }
});
