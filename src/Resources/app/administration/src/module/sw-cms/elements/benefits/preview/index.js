import template from './sw-cms-el-preview-benefits.html.twig';
import './sw-cms-el-preview-benefits.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-benefits', {
    template
});
