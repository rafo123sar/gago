import template from './sw-cms-el-config-info-circles.html.twig';
import './sw-cms-el-config-info-circles.scss';

const { Component, Mixin } = Shopware;

Component.register('sw-cms-el-config-info-circles', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    inject: ['repositoryFactory'],

    data() {
        return {
            mediaModalIsOpen: false,
            initialFolderId: null,
            mediaModalIndex: null,
            configModalIndex: null,
            currentModalConfiguration: null,
            currentModalData: null,
            showModal: false
        };
    },

    computed: {
        mediaRepository() {
            return this.repositoryFactory.create('media');
        }
    },

    created() {
        this.createdComponent();
    },

    methods: {
        uploadTag(index) {
            return `cms-element-media-config-${this.element.id}-${index}`;
        },

        previewSource(index) {
            // if (this.element.data && this.element.data.media && this.element.data.media.id) {
            //     return this.element.data.media;
            // }
            //
            // return this.element.config.media.value;
            if (this.element.data && this.element.data[index] && this.element.data[index].mediaId) {
                return this.element.data[index].mediaId;
            }

            return this.element.config.circles.value[index].media.value;
        },

        addElement() {
            this.element.config.circles.value.push({
                grayText: {
                    source: 'static',
                    value: null
                },
                redText: {
                    source: 'static',
                    value: null
                },
                hasSub:{
                    source: 'static',
                    value: false
                },
                subText:{
                    source: 'static',
                    value: null
                }
            });
            this.$emit('element-update', this.element);
        },

        removeElement(index) {
            if ((index === 0 || index) &&
                this.element.config.circles.value &&
                this.element.config.circles.value[index]
            ) {
                this.element.config.circles.value.splice(index, 1);
            }
        },

        createdComponent() {
            this.initElementConfig('info-circles');
        },

        onImageUpload(index, target) {
            this.mediaRepository.get(target.targetId, Shopware.Context.api).then((mediaEntity) => {
                if ((index === 0 || index) &&
                    this.element.config &&
                    this.element.config.circles.value &&
                    this.element.config.circles.value[index] &&
                    this.element.config.circles.value[index].media
                ) {
                    this.element.config.circles.value[index].media.value = mediaEntity ? mediaEntity.id : null;
                }
                this.updateElementData({'media': mediaEntity, 'index': index});
                this.$emit('element-update', this.element);
            });
        },

        onImageRemove(index) {
            index = (index === 0 || index) ? index : this.mediaModalIndex;
            if (this.element.config &&
                this.element.config.circles.value &&
                this.element.config.circles.value[index] &&
                this.element.config.circles.value[index].media
            ) {
                this.element.config.circles.value[index].media.value = null;
            }

            this.updateElementData({'media': null, 'index': index});

            this.$emit('element-update', this.element);
        },

        onCloseModal() {
            this.mediaModalIsOpen = false;
            this.mediaModalIndex = null;
        },

        onSelectionChanges(mediaEntity) {
            const media = mediaEntity[0];

            if ((this.mediaModalIndex || this.mediaModalIndex == 0) &&
                this.element.config &&
                this.element.config.circles.value &&
                this.element.config.circles.value[this.mediaModalIndex] &&
                this.element.config.circles.value[this.mediaModalIndex].media
            ) {
                this.element.config.circles.value[this.mediaModalIndex].media.value = media ? media.id : null;
                this.element.config.circles.value[this.mediaModalIndex].media.media = media ? media : null;
            }

            this.updateElementData({'media': media, 'index': this.mediaModalIndex});
            this.$emit('element-update', this.element);
        },

        updateElementData(data) {
            if (data.index || data.index == 0) {
                this.element.config.circles.value[data.index].media.value = data.media ? data.media.id : null;
                this.element.config.circles.value[data.index].media.media = data.media ? data.media : null;
                this.$set(this.element.data, data.index, {
                    'media': data.media,
                    'mediaId': data.media ? data.media.id : null
                });
            }
        },

        onOpenMediaModal(index) {
           this.mediaModalIndex = index;
           this.mediaModalIsOpen = true;
        },

        onChangeMinHeight(value) {
            this.$emit('element-update', this.element);
        }
    }
});
