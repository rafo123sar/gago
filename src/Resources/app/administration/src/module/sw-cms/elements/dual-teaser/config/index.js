import template from './sw-cms-el-config-dual-teaser.html.twig';
import './sw-cms-el-config-dual-teaser.scss';

const { Component, Mixin, Context } = Shopware;

Component.register('sw-cms-el-config-dual-teaser', {
    template,

    mixins: [
      Mixin.getByName('cms-element')
    ],

    inject: ['repositoryFactory'],

    computed: {
      mediaRepository() {
        return this.repositoryFactory.create('media');
      },

      controlsCustomImagePreviousUploadTag() {
        return `cms-block-media-config-controls-custom-previous-${ this.element.id }`;
      },

      controlsCustomImageNextUploadTag() {
        return `cms-block-media-config-controls-custom-next-${ this.element.id }`;
      },

      activeSlides() {
        return this.element.config.slides.value.filter((slide) => {
          return slide.active
        });
      }
    },

    data() {
      return {
        slideBackgroundMediaPreviews: [],
        controlsCustomImagePreviousPreview: null,
        controlsCustomImageNextPreview: null
      }
    },

    created() {
      this.createdComponent();
    },

    mounted() {
      this.getMediaEntityPreviews();
    },

    methods: {
      createdComponent() {
        this.initElementConfig('dual-teaser');
      },

      async asyncForEach(array, callback) {
        for (let i = 0; i < array.length; i++) {
          await callback(array[i], i, array);
        }
      },

      async getMediaEntityPreviews() {
        if (this.element.config.slides.value) {
          this.slideBackgroundMediaPreviews = [];

          await this.asyncForEach(this.element.config.slides.value, async (slide) => {
            if (slide.backgroundMedia) {
              const mediaEntity = await this.mediaRepository.get(slide.backgroundMedia, Context.api);

              this.slideBackgroundMediaPreviews.push(mediaEntity);
            } else {
              this.slideBackgroundMediaPreviews.push(null);
            }
          });
        }

        if (this.element.config.settings.value.controlsCustomImagePrevious) {
          const mediaEntity = await this.mediaRepository.get(this.element.config.settings.value.controlsCustomImagePrevious, Context.api);

          this.controlsCustomImagePreviousPreview = mediaEntity
        }

        if (this.element.config.settings.value.controlsCustomImageNext) {
          const mediaEntity = await this.mediaRepository.get(this.element.config.settings.value.controlsCustomImageNext, Context.api);

          this.controlsCustomImageNextPreview = mediaEntity
        }
      },

      async refreshMediaEntityPreviews(mediaEntityId) {
        const mediaEntity = await this.mediaRepository.get(mediaEntityId, Context.api);

        if (this.element.config.slides.value) {
          this.element.config.slides.value.forEach((slide, index) => {
            if (slide.backgroundMedia === mediaEntityId) {
              this.$set(this.slideBackgroundMediaPreviews, index, mediaEntity);
            }
          });
        }

        if (this.element.config.settings.value.controlsCustomImagePrevious === mediaEntityId) {
          this.controlsCustomImagePreviousPreview = mediaEntity
        }

        if (this.element.config.settings.value.controlsCustomImageNext === mediaEntityId) {
          this.controlsCustomImageNextPreview = mediaEntity
        }
      },

      onClickMoveSlide(index, direction) {
        const slides = this.element.config.slides.value;
        let newIndex = null;

        if (direction === 'up') {
          newIndex = index - 1;
        } else if (direction === 'down') {
          newIndex = index + 1;
        }

        if (newIndex !== null) {
          slides.splice(newIndex, 0, slides.splice(index, 1)[0]);
        }

        this.element.config.slides.value = slides;
        this.getMediaEntityPreviews();
      },

      onClickDuplicateSlide(slide) {
        const slides = this.element.config.slides.value;
        const index = slides.indexOf(slide);
        const slideCopy = { ...slide };

        slideCopy.name += ' ' + this.$tc('sw-cms.elements.solid-ase.content-slider.config.slides.copy-slide.suffix');
        slides.splice(index + 1, 0, slideCopy);
        this.element.config.slides.value = slides;
        this.getMediaEntityPreviews();
      },

      onClickRemoveSlide(slide) {
        const slides = this.element.config.slides.value;
        const index = slides.indexOf(slide);

        slides.splice(index, 1);

        if (slides.length === 1) {
          slides[0].active = true;
        }

        this.element.config.slides.value = slides;
        this.getMediaEntityPreviews();
      },

      onRemoveSlideBackgroundMedia(slide) {
        const slides = this.element.config.slides.value;
        const index = slides.indexOf(slide);

        this.$set(this.slideBackgroundMediaPreviews, index, null);
        slide.backgroundMedia = null;
      },

      onChangeSlideBackgroundMedia(mediaEntity, slide) {
        const slides = this.element.config.slides.value;
        const index = slides.indexOf(slide);

        this.$set(this.slideBackgroundMediaPreviews, index, mediaEntity[0]);
        slide.backgroundMedia = mediaEntity[0].id;
      },

      async onFinishUploadSlideBackgroundMedia(mediaItem, slide) {
        const mediaEntity = await this.mediaRepository.get(mediaItem.targetId, Context.api);
        const slides = this.element.config.slides.value;
        const index = slides.indexOf(slide);

        this.$set(this.slideBackgroundMediaPreviews, index, mediaEntity);
        slide.backgroundMedia = mediaEntity.id;
        this.refreshMediaEntityPreviews(mediaEntity.id);
      },

      onRemoveControlsCustomImagePrevious() {
        this.controlsCustomImagePreviousPreview = null;
        this.element.config.settings.value.controlsCustomImagePrevious = null;
      },

      onChangeControlsCustomImagePrevious(mediaEntity) {
        this.controlsCustomImagePreviousPreview = mediaEntity[0];
        this.element.config.settings.value.controlsCustomImagePrevious = mediaEntity[0].id;
      },

      async onFinishUploadControlsCustomImagePrevious(mediaItem) {
        const mediaEntity = await this.mediaRepository.get(mediaItem.targetId, Context.api);

        this.controlsCustomImagePreviousPreview = mediaEntity;
        this.element.config.settings.value.controlsCustomImagePrevious = mediaEntity.id;
        this.refreshMediaEntityPreviews(mediaEntity.id);
      },

      onRemoveControlsCustomImageNext() {
        this.controlsCustomImageNextPreview = null;
        this.element.config.settings.value.controlsCustomImageNext = null;
      },

      onChangeControlsCustomImageNext(mediaEntity) {
        this.controlsCustomImageNextPreview = mediaEntity[0];
        this.element.config.settings.value.controlsCustomImageNext = mediaEntity[0].id;
      },

      async onFinishUploadControlsCustomImageNext(mediaItem) {
        const mediaEntity = await this.mediaRepository.get(mediaItem.targetId, Context.api);

        this.controlsCustomImageNextPreview = mediaEntity;
        this.element.config.settings.value.controlsCustomImageNext = mediaEntity.id;
        this.refreshMediaEntityPreviews(mediaEntity.id);
      }
    }
});