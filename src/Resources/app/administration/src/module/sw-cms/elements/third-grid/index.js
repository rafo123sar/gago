import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'third-grid',
    label: 'Third Grid',
    component: 'sw-cms-el-third-grid',
    configComponent: 'sw-cms-el-config-third-grid',
    previewComponent: 'sw-cms-el-preview-third-grid',
    defaultConfig: {
        headline: {
            source: 'static',
            value: null,
        },
        subline: {
            source: 'static',
            value: null,
        },
        text: {
            source: 'static',
            value: null,
        },
        medias: {
            source: "static",
            value: [
                {
                    media: {
                        source: 'static',
                        value: null,
                        required: false,
                        entity: {
                            name: 'media',
                        }
                    },
                },
                {
                    media: {
                        source: 'static',
                        value: null,
                        required: false,
                        entity: {
                            name: 'media',
                        }
                    },
                },
                {
                    media: {
                        source: 'static',
                        value: null,
                        required: false,
                        entity: {
                            name: 'media',
                        }
                    },
                },
            ]
        }
    },
});
