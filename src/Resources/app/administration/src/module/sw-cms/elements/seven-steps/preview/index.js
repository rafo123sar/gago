import template from './sw-cms-el-preview-seven-steps.html.twig';
import './sw-cms-el-preview-seven-steps.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-seven-steps', {
    template
});
