import template from './sw-cms-el-newsletter.html.twig';
import './sw-cms-el-newsletter.scss';

const { Component, Mixin, Filter, Utils } = Shopware;

Component.register('sw-cms-el-newsletter', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    computed: {

        mediaUrl() {
            const elemData = this.element.data.media;
            const mediaSource = this.element.config.media.source;

            if (mediaSource === 'mapped') {
                const demoMedia = this.getDemoValue(this.element.config.media.value);

                if (demoMedia && demoMedia.url) {
                    return demoMedia.url;
                }

                return null
            }

            if (elemData && elemData.id) {
                return this.element.data.media.url;
            }

            if (elemData && elemData.url) {
                return this.assetFilter(elemData.url);
            }

            return null;
        },

        assetFilter() {
            return Filter.getByName('asset');
        },

        mediaConfigValue() {
            return Utils.get(this.element, 'config.sliderItems.value');
        }
    },

    watch: {
        cmsPageState: {
            deep: true,
            handler() {
                this.$forceUpdate();
            }
        },

        mediaConfigValue(value) {
            const mediaId = Utils.get(this.element, 'data.media.id');
            const isSourceStatic = Utils.get(this.element, 'config.media.source') === 'static';

            if (isSourceStatic && mediaId && value !== mediaId) {
                this.element.config.media.value = mediaId;
            }
        }
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('newsletter');
            this.initElementData('newsletter');
        }
    }
});
