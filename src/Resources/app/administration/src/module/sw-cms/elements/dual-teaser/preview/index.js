import template from './sw-cms-el-preview-dual-teaser.html.twig';
import './sw-cms-el-preview-dual-teaser.scss';

Shopware.Component.register('sw-cms-el-preview-dual-teaser', {
  template
});