import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'text-bullets',
    label: 'Text and Bullets',
    component: 'sw-cms-el-text-bullets',
    configComponent: 'sw-cms-el-config-text-bullets',
    previewComponent: 'sw-cms-el-preview-text-bullets',
    defaultConfig: {
        header:{
            source: 'static',
            value: null
        },
        description:{
            source: 'static',
            value: null
        },
        title:{
            source: 'static',
            value: null
        },
        bullets: {
            source: 'static',
            value: [{
                content: {
                    source: 'static',
                    value: null
                }
            }]
        }
    }
});
