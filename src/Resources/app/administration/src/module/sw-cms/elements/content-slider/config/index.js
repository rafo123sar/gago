import template from './sw-cms-el-config-content-slider.html.twig';
import './sw-cms-el-config-content-slider.scss';

const {Component, Mixin} = Shopware;

Component.register('sw-cms-el-config-content-slider', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    inject: ['repositoryFactory'],

    data() {
        return {
            mediaModalIsOpen: false,
            initialFolderId: null,
            mediaModalIndex: null,
            configModalIndex: null,
            currentModalConfiguration: null,
            currentModalData: null,
            showModal: false
        };
    },

    computed: {
        mediaRepository() {
            return this.repositoryFactory.create('media');
        }
    },

    created() {
        this.createdComponent();
    },

    methods: {
        uploadTag(index) {
            return `cms-element-media-config-${this.element.id}-${index}`;
        },

        previewSource(index) {
            // if (this.element.data && this.element.data.media && this.element.data.media.id) {
            //     return this.element.data.media;
            // }
            //
            // return this.element.config.media.value;

            let num = index.substr(index.length - 1)

            if (index.indexOf('main')) {
                if (this.element.data && this.element.data[num] && this.element.data[num].mediaId) {
                    return this.element.data[num].mediaId;
                }

                return this.element.config.sliders.value[num].media.value;

            } else if (index.indexOf('second')) {
                  if (this.element.data && this.element.data[num] && this.element.data[num].mediaId) {
                    return this.element.data[num].mediaId;
                }

                return this.element.config.sliders.value[num].media2.value;
            }

            return
        },

        addElement() {
            this.element.config.sliders.value.push({
                media: {
                    source: 'static',
                    value: null,
                    required: true,
                    entity: {
                        name: 'media'
                    }
                },
                headline: {
                    source: 'static',
                    value: null
                },
                topic: {
                    source: 'static',
                    value: null
                },
                subline: {
                    source: 'static',
                    value: null
                },
                buttonText:{
                    source: 'static',
                    value: '',
                },
                buttonTitle:{
                    source: 'static',
                    value: '',
                },
                url: {
                    source: 'static',
                    value: '',
                },
                showRight:{
                    source: 'static',
                    value: true,
                },
                media2: {
                    source: 'static',
                    value: null,
                    required: true,
                    entity: {
                        name: 'media'
                    }
                },
                headline2: {
                    source: 'static',
                    value: null
                },

                description2: {
                    source: 'static',
                    value: null
                },

                buttonTitle2:{
                    source: 'static',
                    value: ''
                },

                url2: {
                    source: 'static',
                    value: ''
                }
            });
            this.$emit('element-update', this.element);
        },

        removeElement(index) {

            if ((index === 0 || index) &&
                this.element.config.sliders.value &&
                this.element.config.sliders.value[index]
            ) {
                this.element.config.sliders.value.splice(index, 1);
            }
        },

        createdComponent() {
            this.initElementConfig('content-slider');
        },

        onImageUpload(index, target) {
            let num = index.substr(index.length - 1);

            if (index.indexOf('main')) {
                this.mediaRepository.get(target.targetId, Shopware.Context.api).then((mediaEntity) => {
                    if ((num === 0 || num) &&
                        this.element.config &&
                        this.element.config.sliders.value &&
                        this.element.config.sliders.value[num] &&
                        this.element.config.sliders.value[num].media
                    ) {
                        this.element.config.sliders.value[num].media.value = mediaEntity ? mediaEntity.id : null;
                    }
                    this.updateElementData({'media': mediaEntity, 'index': num});
                    this.$emit('element-update', this.element);
                });
            }
            else if(index.indexOf('second')){
                this.mediaRepository.get(target.targetId, Shopware.Context.api).then((mediaEntity) => {
                    if ((num === 0 || num) &&
                        this.element.config &&
                        this.element.config.sliders.value &&
                        this.element.config.sliders.value[num] &&
                        this.element.config.sliders.value[num].media2
                    ) {
                        this.element.config.sliders.value[num].media2.value = mediaEntity ? mediaEntity.id : null;
                    }
                    this.updateElementData({'media': mediaEntity, 'index': num});
                    this.$emit('element-update', this.element);
                });
            }

        },


        onImageRemove(index) {
            index = (index === 0 || index) ? index : this.mediaModalIndex;
            if (this.element.config &&
                this.element.config.sliders.value &&
                this.element.config.sliders.value[index] &&
                this.element.config.sliders.value[index].media
            ) {
                this.element.config.sliders.value[index].media.value = null;
            }

            this.updateElementData({'media': null, 'index': index});

            this.$emit('element-update', this.element);
        },


        onCloseModal() {
            this.mediaModalIsOpen = false;
            this.mediaModalIndex = null;
        },


        onSelectionChanges(mediaEntity) {
            const media = mediaEntity[0];



            if ((this.mediaModalIndex || this.mediaModalIndex == 0) &&
                this.element.config &&
                this.element.config.sliders.value &&
                this.element.config.sliders.value[this.mediaModalIndex] &&
                this.element.config.sliders.value[this.mediaModalIndex].media
            ) {
                this.element.config.sliders.value[this.mediaModalIndex].media.value = media ? media.id : null;
                this.element.config.sliders.value[this.mediaModalIndex].media.media = media ? media : null;
            }

            this.updateElementData({'media': media, 'index': this.mediaModalIndex});
            this.$emit('element-update', this.element);
        },


        updateElementData(data) {

            let num = data.index.substr(data.index.length - 1);
            if (data.index.indexOf('main')){
                if (num || num == 0) {
                    this.element.config.sliders.value[num].media.value = data.media ? data.media.id : null;
                    this.element.config.sliders.value[num].media.media = data.media ? data.media : null;
                    // this.$set(this.element.data, num, {
                    //     'media': data.media,
                    //     'mediaId': data.media ? data.media.id : null
                    // });
                }
            }
            else if(data.index.indexOf('second')){
                if (num || num == 0) {
                    this.element.config.sliders.value[num].media2.value = data.media ? data.media.id : null;
                    this.element.config.sliders.value[num].media2.media = data.media ? data.media : null;
                    // this.$set(this.element.data, num, {
                    //     'media': data.media,
                    //     'mediaId': data.media ? data.media.id : null
                    // });
                }
            }

        },

        updateElementData2(data) {

            if (data.index || data.index == 0) {
                this.element.config.sliders.value[data.index].media2.value = data.media ? data.media.id : null;
                this.element.config.sliders.value[data.index].media2.media = data.media ? data.media : null;
                this.$set(this.element.data, data.index, {
                    'media': data.media,
                    'mediaId': data.media ? data.media.id : null
                });
            }
        },

        onOpenMediaModal(index) {
            this.mediaModalIndex = index;
            this.mediaModalIsOpen = true;
        },


        onChangeMinHeight(value) {
            this.$emit('element-update', this.element);
        }
    }
});
