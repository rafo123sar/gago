import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'benefits',
    label: 'Benefits',
    component: 'sw-cms-el-benefits',
    configComponent: 'sw-cms-el-config-benefits',
    previewComponent: 'sw-cms-el-preview-benefits',
    defaultConfig: {
        media: {
            source: 'static',
            value: null,
            required: true,
            entity: {
                name: 'media'
            }
        },
        headline1:{
            source: 'static',
            value: null
        },
        headline2:{
            source: 'static',
            value: null
        },
        subLine1:{
            source: 'static',
            value: null
        },
        subLine2:{
            source: 'static',
            value: null
        },
        description1:{
            source:'static',
            value: null
        },
        description2:{
            source: 'static',
            value: null
        },
        btnUrl:{
            source: 'static',
            value: null
        },
        btnTitle: {
            source: 'static',
            value: null
        },
        advantage1: {
            source: 'static',
            value: null
        },
        advantage2: {
            source: 'static',
            value: null
        },
        advantage3: {
            source: 'static',
            value: null
        },
        advantage4: {
            source: 'static',
            value: null
        },
        advantage5: {
            source: 'static',
            value: null
        }

    }
});
