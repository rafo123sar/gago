import template from './sw-cms-el-preview-sps-slider.html.twig';
import './sw-cms-el-preview-sps-slider.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-sps-slider', {
    template
});
