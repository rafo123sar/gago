import template from './sw-cms-el-preview-info-circles.html.twig';
import './sw-cms-el-preview-info-circles.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-info-circles', {
    template
});
