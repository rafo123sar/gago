import template from './sw-cms-el-preview-banner-text.html.twig';
import './sw-cms-el-preview-banner-text.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-banner-text', {
    template
});
