import template from './sw-cms-el-preview-text-bullets.html.twig';
import './sw-cms-el-preview-text-bullets.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-text-bullets', {
    template
});
