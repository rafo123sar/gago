import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'content-slider',
    label: 'Services',
    component: 'sw-cms-el-content-slider',
    configComponent: 'sw-cms-el-config-content-slider',
    previewComponent: 'sw-cms-el-preview-content-slider',
    defaultConfig: {

        sliders: {
            source: 'static',
            value: [{
                media: {
                    source: 'static',
                    value: null,
                    required: true,
                    entity: {
                        name: 'media'
                    }
                },
                headline: {
                    source: 'static',
                    value: null
                },
                topic: {
                    source: 'static',
                    value: null
                },
                subline: {
                    source: 'static',
                    value: null
                },
                buttonText:{
                    source: 'static',
                    value: '',
                },
                buttonTitle:{
                    source: 'static',
                    value: '',
                },
                url: {
                    source: 'static',
                    value: '',
                },

                showRight:{
                    source: 'static',
                    value: true,
                },

                media2: {
                    source: 'static',
                    value: null,
                    required: true,
                    entity: {
                        name: 'media'
                    }
                },

                headline2: {
                    source: 'static',
                    value: null
                },

                description2: {
                    source: 'static',
                    value: null
                },
                buttonTitle2:{
                    source: 'static',
                    value: ''
                },
                url2: {
                    source: 'static',
                    value: ''
                }
            }]
        }
    }
});
