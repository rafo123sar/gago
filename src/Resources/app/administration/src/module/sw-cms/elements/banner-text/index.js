import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'banner-text',
    label: 'Banner & Text',
    component: 'sw-cms-el-banner-text',
    configComponent: 'sw-cms-el-config-banner-text',
    previewComponent: 'sw-cms-el-preview-banner-text',
    defaultConfig: {
        media: {
            source: 'static',
            value: null,
            required: true,
            entity: {
                name: 'media'
            }
        },
        headline: {
            source: 'static',
            value: null
        },
        topic: {
            source: 'static',
            value: null
        },
        subline: {
            source: 'static',
            value: null
        },
        buttonText:{
            source: 'static',
            value: '',
        },
        buttonTitle:{
            source: 'static',
            value: '',
        },
        url: {
            source: 'static',
            value: '',
        },
        newTab: {
            source: 'static',
            value: false,
        },
        btnAlignment: {
            source: 'static',
            value: "left",
        },
        skewDesign : {
            source: 'static',
            value: 'left-top'
        }
    }
});
