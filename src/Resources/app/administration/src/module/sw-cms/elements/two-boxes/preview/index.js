import template from './sw-cms-el-preview-two-boxes.html.twig';
import './sw-cms-el-preview-two-boxes.scss';

Shopware.Component.register('sw-cms-el-preview-two-boxes', {
  template
});