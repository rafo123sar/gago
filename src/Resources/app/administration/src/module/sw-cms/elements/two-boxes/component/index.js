import template from './sw-cms-el-two-boxes.html.twig';
import './sw-cms-el-two-boxes.scss';

const { Component, Mixin } = Shopware;

Component.register('sw-cms-el-two-boxes', {
  template,

  mixins: [
    Mixin.getByName('cms-element')
  ],

  created() {
    this.createdComponent();
  },

  methods: {
    createdComponent() {
      this.initElementConfig('two-boxes');
      this.initElementData('two-boxes');
    }
  }
});