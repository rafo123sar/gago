import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'info-circles',
    label: 'Info circles',
    component: 'sw-cms-el-info-circles',
    configComponent: 'sw-cms-el-config-info-circles',
    previewComponent: 'sw-cms-el-preview-info-circles',
    defaultConfig: {
        circles: {
            source: 'static',
            value: [{
                grayText: {
                    source: 'static',
                    value: null
                },
                redText: {
                    source: 'static',
                    value: null
                },
                hasSub:{
                    source: 'static',
                    value: false
                },
                subText:{
                    source: 'static',
                    value: null
                }
            }]
        }
    }
});
