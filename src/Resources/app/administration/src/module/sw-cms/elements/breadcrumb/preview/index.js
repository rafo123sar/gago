import template from './sw-cms-el-preview-breadcrumb.html.twig';
import './sw-cms-el-preview-breadcrumb.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-breadcrumb', {
    template,
});
