import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'seven-steps',
    label: 'Seven steps',
    component: 'sw-cms-el-seven-steps',
    configComponent: 'sw-cms-el-config-seven-steps',
    previewComponent: 'sw-cms-el-preview-seven-steps',
    defaultConfig: {
        header:{
            source: 'static',
            value: null
        },
        supText:{
            source: 'static',
            value: null
        },
        media2: {
            source: 'static',
            value: null,
            required: true,
            entity: {
                name: 'media'
            }
        },
        headline1: {
            source: 'static',
            value: null
        },
        description1:{
            source: 'static',
            value: null
        },
        sliders: {
            source: 'static',
            value: [{
                media: {
                    source: 'static',
                    value: null,
                    required: true,
                    entity: {
                        name: 'media'
                    }
                },
                headline: {
                    source: 'static',
                    value: null
                },

                description: {
                    source: 'static',
                    value: null
                }
            }]
        }
    }
});
