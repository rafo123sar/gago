import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'dialog',
    label: 'dialog',
    component: 'sw-cms-el-dialog',
    configComponent: 'sw-cms-el-config-dialog',
    previewComponent: 'sw-cms-el-preview-dialog',
    defaultConfig: {
        headline: {
            source: 'static',
            value: '',
        },
        subLine:{
            source: 'static' ,
            value: ''
        },
        description: {
            source: 'static',
            value: ''
        },
        buttonTitle:{
            source: 'static',
            value: 'Weiter',
        },
        url: {
            source: 'static',
            value: '',
        }
    },
});
