import template from './sw-cms-el-preview-third-grid.html.twig';
import './sw-cms-el-preview-third-grid.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-third-grid', {
    template,
});
