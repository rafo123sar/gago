import template from './sw-cms-el-preview-subcategory-teaser.html.twig';
import './sw-cms-el-preview-subcategory-teaser.scss';

Shopware.Component.register('sw-cms-el-preview-subcategory-teaser', {
  template
});