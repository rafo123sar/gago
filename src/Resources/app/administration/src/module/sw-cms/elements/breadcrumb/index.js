import './component';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'breadcrumb',
    label: 'breadcrumb',
    component: 'sw-cms-el-breadcrumb',
    configComponent: 'sw-cms-el-config-breadcrumb',
    previewComponent: 'sw-cms-el-preview-breadcrumb'
});
