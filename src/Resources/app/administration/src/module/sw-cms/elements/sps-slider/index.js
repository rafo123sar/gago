import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'sps-slider',
    label: 'Sps image slider',
    component: 'sw-cms-el-sps-slider',
    configComponent: 'sw-cms-el-config-sps-slider',
    previewComponent: 'sw-cms-el-preview-sps-slider',
    defaultConfig: {

        sliders: {
            source: 'static',
            value: [{
                media: {
                    source: 'static',
                    value: null,
                    required: true,
                    entity: {
                        name: 'media'
                    }
                },
                title: {
                    source: 'static',
                    value: null
                }
            }]
        }
    }
});
