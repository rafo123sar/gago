import template from './sw-cms-el-preview-services.html.twig';
import './sw-cms-el-preview-services.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-services', {
    template
});
