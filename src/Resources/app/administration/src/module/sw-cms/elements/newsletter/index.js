import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'newsletter',
    label: 'Newsletter',
    component: 'sw-cms-el-newsletter',
    configComponent: 'sw-cms-el-config-newsletter',
    previewComponent: 'sw-cms-el-preview-newsletter',
    defaultConfig: {
        media: {
            source: 'static',
            value: null,
            entity: {
                name: 'media'
            }
        },
        headline: {
            source: 'static',
            value: null
        },
        subline: {
            source: 'static',
            value: null
        },
        text: {
            source: 'static',
            value: null
        },
        privacyText: {
            source: 'static',
            value: null
        },
        buttonText:{
            source: 'static',
            value: '',
        },
        employeePhone:{
            source: 'static',
            value: '',
        },
        employeeTitle:{
            source: 'static',
            value: '',
        },
        employeeName: {
            source: 'static',
            value: '',
        }
    }
});
