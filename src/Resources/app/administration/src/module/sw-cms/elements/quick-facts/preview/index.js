import template from './sw-cms-el-preview-quick-facts.html.twig';
import './sw-cms-el-preview-quick-facts.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-quick-facts', {
    template,
});
