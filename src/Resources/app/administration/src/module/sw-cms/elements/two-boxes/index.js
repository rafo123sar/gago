import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
  name: 'two-boxes',
  label: 'Two Boxes',
  component: 'sw-cms-el-two-boxes',
  configComponent: 'sw-cms-el-config-two-boxes',
  previewComponent: 'sw-cms-el-preview-two-boxes',
  defaultConfig: {
    slides: {
      source: 'static',
      value: [
        {
          active: true,
          name: 'Content 1',
          contentType: 'default',

          // content
          headline: 'Lorem ipsum dolor sit amet',
          text: 'Asequat massa quis enim. Donec pednsequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venene justo, fringilla vel, aliquet nec.',
          buttonLink: '#',
          buttonTargetBlank: false,
          customContent: '',

          // background
          backgroundColor: '',
          backgroundMedia: null,
          backgroundSizingMode: 'contain',
          backgroundPosition: 'top',
          backgroundAnimation: '',

          // link
          link: '',
          linkTargetBlank: false
        },
        {
          active: true,
          name: 'Content 2',
          contentType: 'default',

          // content
          headline: 'Lorem ipsum dolor sit amet',
          text: 'Asequat massa quis enim. Donec pednsequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venene justo, fringilla vel, aliquet nec.',
          buttonLink: '#',
          buttonTargetBlank: false,
          customContent: '',

          // background
          backgroundColor: '',
          backgroundMedia: null,
          backgroundSizingMode: 'contain',
          backgroundPosition: 'top',
          backgroundAnimation: '',

          // link
          link: '',
          linkTargetBlank: false
        }
      ]
    },
    sliderSettings: {
      source: 'static',
      value: {
        // general
        header: '',
        animation: 'slider-horizontal-ease-in-out-sine',
        items: '2',
        startIndex: '0',
        speed: '500',
        slideBy: '1',
        gutter: '0',
        loop: false,
        rewind: false,

        // navigation
        controls: false,
        mouseDrag: false,
        nav: false,

        // autoplay
        autoplay: false,
        autoplayDirection: 'forward',
        autoplayHoverpause: false,
        autoplayTimeout: '4000',
      }
    },
    settings: {
      source: 'static',
      value: {
        // general
        minHeightMobile: '240px',
        minHeightTablet: '240px',
        minHeightDesktop: '240px',

        // content
        layoutVariant: 'gradient-bottom',
        contentAnimation: 'fade-in-up-ease',
        smallHeadlineSizeMobile: '',
        smallHeadlineSizeTablet: '',
        smallHeadlineSizeDesktop: '',
        smallHeadlineWeight: '',
        headlineSizeMobile: '',
        headlineSizeTablet: '',
        headlineSizeDesktop: '',
        headlineWeight: '',
        textSizeMobile: '',
        textSizeTablet: '',
        textSizeDesktop: '',
        textWeight: '',
        buttonLabelSizeMobile: '',
        buttonLabelSizeTablet: '',
        buttonLabelSizeDesktop: '',
        buttonLabelWeight: '',
        buttonVariant: 'primary',
        smallHeadlineMarginBottomMobile: '',
        smallHeadlineMarginBottomTablet: '',
        smallHeadlineMarginBottomDesktop: '',
        headlineMarginBottomMobile: '',
        headlineMarginBottomTablet: '',
        headlineMarginBottomDesktop: '',
        textMarginBottomMobile: '',
        textMarginBottomTablet: '',
        textMarginBottomDesktop: '',
        smallHeadlineColor: '',
        headlineColor: '',
        textColor: '',
        buttonColor: '',
        contentBackgroundColor: '',

        // navigation
        controlsVariant: 'icon',
        controlsIconVariant: 'arrow',
        controlsPosition: 'horizontal-inside-center-edges',
        controlsColor: '#4dad2e',
        controlsCustomImagePrevious: null,
        controlsCustomImageNext: null,
        navVariant: 'dots-opacity',
        navSize: 'medium',
        navPosition: 'horizontal-bottom-center',
        navColor: '#4dad2e',

        // custom
        customCss: ''
      }
    }
  }
});
