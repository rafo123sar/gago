import template from './sw-cms-el-preview-dual-text-image.html.twig';
import './sw-cms-el-preview-dual-text-image.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-dual-text-image', {
    template
});
