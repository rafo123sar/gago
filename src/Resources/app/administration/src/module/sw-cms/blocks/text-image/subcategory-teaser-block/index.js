import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'subcategory-teaser-block',
    label: 'Subcategories Teasers',
    category: 'text-image',
    component: 'sw-cms-block-subcategory-teaser',
    previewComponent: 'sw-cms-preview-subcategory-teaser',
    defaultConfig: {
        marginBottom: '0px',
        marginTop: '0px',
        marginLeft: '0px',
        marginRight: '0px',
        sizingMode: 'boxed'
    },
    slots: {
        subcategoryteaser: 'subcategory-teaser'
    }
});
