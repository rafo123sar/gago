import template from './sw-cms-preview-dialog.html.twig';
import './sw-cms-preview-dialog.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-dialog', {
    template,
});
