import template from './sw-cms-block-banner-text.html.twig';
import './sw-cms-block-banner-text.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-banner-text', {
    template
});
