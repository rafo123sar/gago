import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'third-grid-block',
    label: 'Third Grid',
    category: 'text-image',
    component: 'sw-cms-block-third-grid',
    previewComponent: 'sw-cms-preview-third-grid',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed',
    },
    slots: {
        thirdgrid: {
            type: 'third-grid',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'standard' },
                },
                data: {
                    media: {
                        url: '/spstheme/img/custom-elements/third-grid.png',
                    },
                },
            },
        },

    },
});
