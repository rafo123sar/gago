import template from './sw-cms-block-subcategory-teaser.html.twig';
import './sw-cms-block-subcategory-teaser.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-subcategory-teaser', {
    template
});
