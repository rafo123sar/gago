import template from './sw-cms-block-breadcrumb.html.twig';

const { Component } = Shopware;

Component.register('sw-cms-block-breadcrumb', {
    template,
});
