import template from './sw-cms-block-dialog.html.twig';

const { Component } = Shopware;

Component.register('sw-cms-block-dialog', {
    template,
});
