import template from './sw-cms-block-dual-text-image.html.twig';
import './sw-cms-block-dual-text-image.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-dual-text-image', {
    template
});
