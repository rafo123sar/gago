import template from './sw-cms-preview-text-bullets.html.twig';
import './sw-cms-preview-text-bullets.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-text-bullets', {
    template,
});
