import template from './sw-cms-preview-seven-steps.html.twig';
import './sw-cms-preview-seven-steps.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-seven-steps', {
    template
});
