import template from './sw-cms-block-preview-dual-teaser.html.twig';
import './sw-cms-block-preview-dual-teaser.scss';

Shopware.Component.register('sw-cms-block-preview-dual-teaser', {
  template
});
