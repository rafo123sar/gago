import template from './sw-cms-block-product-grid.html.twig';

const { Component } = Shopware;

Component.register('sw-cms-block-product-grid', {
    template
});
