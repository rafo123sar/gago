import template from './sw-cms-block-content-slider.html.twig';
import './sw-cms-block-content-slider.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-content-slider', {
    template
});
