import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'dialog-block',
    label: 'Dialog text ',
    category: 'text',
    component: 'sw-cms-block-dialog',
    previewComponent: 'sw-cms-preview-dialog',
    defaultConfig: {
        marginBottom: '0px',
        marginTop: '0px',
        marginLeft: '0px',
        marginRight: '0px',
        sizingMode: 'full_width',
    },
    slots: {
        dialog: 'dialog',
    },
});
