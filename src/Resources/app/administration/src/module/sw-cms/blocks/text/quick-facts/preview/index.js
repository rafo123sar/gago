import template from './sw-cms-preview-quick-facts.html.twig';
import './sw-cms-preview-quick-facts.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-quick-facts', {
    template,
});
