import template from './sw-cms-block-quick-facts.html.twig';

const { Component } = Shopware;

Component.register('sw-cms-block-quick-facts', {
    template,
});
