import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'info-circles-block',
    label: 'Info circles',
    category: 'text',
    component: 'sw-cms-block-info-circles',
    previewComponent: 'sw-cms-preview-info-circles',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '0px',
        marginRight: '0px',
        sizingMode: 'boxed',
    },
    slots: {
        infocircles: 'info-circles',
    },
});
