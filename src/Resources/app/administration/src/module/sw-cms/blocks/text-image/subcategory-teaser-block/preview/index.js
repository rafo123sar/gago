import template from './sw-cms-preview-subcategory-teaser.html.twig';
import './sw-cms-preview-subcategory-teaser.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-subcategory-teaser', {
    template
});
