import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'text-bullets-block',
    label: 'Text and bullets',
    category: 'text',
    component: 'sw-cms-block-text-bullets',
    previewComponent: 'sw-cms-preview-text-bullets',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed',
    },
    slots: {
        textbullets: 'text-bullets',
    },
});
