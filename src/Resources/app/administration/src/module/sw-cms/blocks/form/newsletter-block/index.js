import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'newsletter-block',
    label: 'Newsletter',
    category: 'form',
    component: 'sw-cms-block-newsletter',
    previewComponent: 'sw-cms-preview-newsletter',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
        newsletter: 'newsletter'
    }
});
