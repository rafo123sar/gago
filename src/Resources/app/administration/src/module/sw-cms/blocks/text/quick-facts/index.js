import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'quick-facts',
    label: 'Quick facts',
    category: 'text',
    component: 'sw-cms-block-quick-facts',
    previewComponent: 'sw-cms-preview-quick-facts',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed',
    },
    slots: {
        quickfacts: 'quick-facts',
    },
});
