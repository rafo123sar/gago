import template from './sw-cms-block-services.html.twig';
import './sw-cms-block-services.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-services', {
    template
});
