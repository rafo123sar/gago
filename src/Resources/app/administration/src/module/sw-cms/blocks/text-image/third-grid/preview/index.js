import template from './sw-cms-preview-third-grid.html.twig';
import './sw-cms-preview-third-grid.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-third-grid', {
    template,
});
