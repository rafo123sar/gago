import template from './sw-cms-preview-info-circles.html.twig';
import './sw-cms-preview-info-circles.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-info-circles', {
    template,
});
