import template from './sw-cms-preview-sps-slider.html.twig';
import './sw-cms-preview-sps-slider.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-sps-slider', {
    template,
});
