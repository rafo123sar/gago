import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'product-grid-block',
    label: 'Product grid',
    category: 'commerce',
    component: 'sw-cms-block-product-grid',
    previewComponent: 'sw-cms-preview-product-grid',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
       productgrid : 'product-grid'
    }
});
