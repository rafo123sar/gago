import template from './sw-cms-block-benefits.html.twig';
import './sw-cms-block-benefits.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-benefits', {
    template
});
