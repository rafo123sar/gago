import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'seven-steps-block',
    label: 'Seven Steps',
    category: 'text-image',
    component: 'sw-cms-block-seven-steps',
    previewComponent: 'sw-cms-preview-seven-steps',
    defaultConfig: {
        marginBottom: '0px',
        marginTop: '0px',
        marginLeft: '0px',
        marginRight: '0px',
        sizingMode: 'boxed'
    },
    slots: {
        sevensteps: 'seven-steps'
    }
});
