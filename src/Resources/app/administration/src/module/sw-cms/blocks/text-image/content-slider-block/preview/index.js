import template from './sw-cms-preview-content-slider.html.twig';
import './sw-cms-preview-content-slider.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-content-slider', {
    template
});
