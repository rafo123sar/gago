import template from './sw-cms-preview-newsletter.html.twig';
import './sw-cms-preview-newsletter.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-newsletter', {
    template
});
