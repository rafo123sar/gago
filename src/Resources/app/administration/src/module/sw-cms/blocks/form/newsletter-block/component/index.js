import template from './sw-cms-block-newsletter.html.twig';
import './sw-cms-block-newsletter.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-newsletter', {
    template
});
