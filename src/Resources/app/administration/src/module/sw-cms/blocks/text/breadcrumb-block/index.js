import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'breadcrumb-block',
    label: 'Breadcrumb element',
    category: 'text',
    component: 'sw-cms-block-breadcrumb',
    previewComponent: 'sw-cms-preview-breadcrumb',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '0px',
        marginRight: '0px',
        sizingMode: 'boxed',
    },
    slots: {
        breadcrumb: 'breadcrumb',
    },
});
