import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'services-block',
    label: 'Services',
    category: 'text-image',
    component: 'sw-cms-block-services',
    previewComponent: 'sw-cms-preview-services',
    defaultConfig: {
        marginBottom: '0px',
        marginTop: '0px',
        marginLeft: '0px',
        marginRight: '0px',
        sizingMode: 'boxed'
    },
    slots: {
        services: {
            type: 'services' ,
            default: {
                config: {
                    displayMode: { source: 'static', value: 'standard' }
                },
                data: {
                    media: {
                        url: '/spstheme/img/custom-elements/services.png'
                    }
                }
            }
        }
    }
});
