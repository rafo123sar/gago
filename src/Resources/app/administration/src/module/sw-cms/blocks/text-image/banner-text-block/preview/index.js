import template from './sw-cms-preview-banner-text.html.twig';
import './sw-cms-preview-banner-text.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-banner-text', {
    template
});
