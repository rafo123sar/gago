import template from './sw-cms-preview-dual-text-image.html.twig';
import './sw-cms-preview-dual-text-image.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-dual-text-image', {
    template
});
