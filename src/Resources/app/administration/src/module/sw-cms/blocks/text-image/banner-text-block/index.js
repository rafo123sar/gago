import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'banner-text-block',
    label: 'Banner & Text',
    category: 'text-image',
    component: 'sw-cms-block-banner-text',
    previewComponent: 'sw-cms-preview-banner-text',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
        bannertext: 'banner-text'
    }
});
