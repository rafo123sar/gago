import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'content-slider-block',
    label: 'Content slider',
    category: 'text-image',
    component: 'sw-cms-block-content-slider',
    previewComponent: 'sw-cms-preview-content-slider',
    defaultConfig: {
        marginBottom: '0',
        marginTop: '0',
        marginLeft: '0',
        marginRight: '0',
        sizingMode: 'boxed'
    },
    slots: {
        contentslider: 'content-slider'
    }
});
