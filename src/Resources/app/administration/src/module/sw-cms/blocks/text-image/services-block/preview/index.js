import template from './sw-cms-preview-services.html.twig';
import './sw-cms-preview-services.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-services', {
    template
});
