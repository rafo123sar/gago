import template from './sw-cms-preview-benefits.html.twig';
import './sw-cms-preview-benefits.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-benefits', {
    template
});
