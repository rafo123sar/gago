import template from './sw-cms-block-text-bullets.html.twig';

const { Component } = Shopware;

Component.register('sw-cms-block-text-bullets', {
    template,
});
