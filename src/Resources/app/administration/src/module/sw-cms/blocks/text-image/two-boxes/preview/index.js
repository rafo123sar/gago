import template from './sw-cms-block-preview-two-boxes.html.twig';
import './sw-cms-block-preview-two-boxes.scss';

Shopware.Component.register('sw-cms-block-preview-two-boxes', {
  template
});
