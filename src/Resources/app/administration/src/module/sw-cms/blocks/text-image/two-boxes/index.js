import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
  name: 'two-boxes-block',
  label: 'Two Boxes',
  category: 'text-image',
  component: 'sw-cms-block-two-boxes',
  previewComponent: 'sw-cms-block-preview-two-boxes',
  defaultConfig: {
    marginBottom: '',
    marginTop: '',
    marginLeft: '',
    marginRight: '',
    sizingMode: 'boxed'
  },
  slots: {
    twoboxes: {
      type: 'two-boxes'
    }
  }
});