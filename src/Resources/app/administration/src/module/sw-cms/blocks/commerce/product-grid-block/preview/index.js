import template from './sw-cms-preview-product-grid.html.twig';
import './sw-cms-preview-product-grid.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-product-grid', {
    template
});
