import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'sps-slider-block',
    label: 'Sps image slider',
    category: 'image',
    component: 'sw-cms-block-sps-slider',
    previewComponent: 'sw-cms-preview-sps-slider',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed',
    },
    slots: {
        spsslider: 'sps-slider',
    },
});
