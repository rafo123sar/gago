import template from './sw-cms-block-info-circles.html.twig';

const { Component } = Shopware;

Component.register('sw-cms-block-info-circles', {
    template,
});
