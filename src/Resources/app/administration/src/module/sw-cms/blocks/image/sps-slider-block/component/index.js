import template from './sw-cms-block-sps-slider.html.twig';

const { Component } = Shopware;

Component.register('sw-cms-block-sps-slider', {
    template,
});
