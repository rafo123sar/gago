import template from './sw-cms-block-third-grid.html.twig';

const { Component } = Shopware;

Component.register('sw-cms-block-third-grid', {
    template,
});
