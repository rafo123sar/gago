import template from './sw-cms-block-seven-steps.html.twig';
import './sw-cms-block-seven-steps.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-seven-steps', {
    template
});
