import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'benefits-block',
    label: 'Benefits',
    category: 'text-image',
    component: 'sw-cms-block-benefits',
    previewComponent: 'sw-cms-preview-benefits',
    defaultConfig: {
        marginBottom: '0px',
        marginTop: '0px',
        marginLeft: '0px',
        marginRight: '0px',
        sizingMode: 'boxed'
    },
    slots: {
        benefits: {
            type: 'benefits' ,
            default: {
                config: {
                    displayMode: { source: 'static', value: 'standard' }
                },
                data: {
                    media: {
                        url: '/spstheme/img/custom-elements/benefits.png'
                    }
                }
            }
        }
    }
});
