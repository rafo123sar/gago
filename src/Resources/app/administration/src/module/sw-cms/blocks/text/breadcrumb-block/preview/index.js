import template from './sw-cms-preview-breadcrumb.html.twig';
import './sw-cms-preview-breadcrumb.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-breadcrumb', {
    template,
});
