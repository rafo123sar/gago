import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'dual-text-image-block',
    label: 'Text block with wide Image',
    category: 'text-image',
    component: 'sw-cms-block-dual-text-image',
    previewComponent: 'sw-cms-preview-dual-text-image',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
        dualtextimage: 'dual-text-image'
    }
});
