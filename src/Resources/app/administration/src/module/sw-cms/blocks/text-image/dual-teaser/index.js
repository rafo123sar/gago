import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
  name: 'dual-teaser-block',
  label: 'Dual Teaser',
  category: 'text-image',
  component: 'sw-cms-block-dual-teaser',
  previewComponent: 'sw-cms-block-preview-dual-teaser',
  defaultConfig: {
    marginBottom: '',
    marginTop: '',
    marginLeft: '',
    marginRight: '',
    sizingMode: 'boxed'
  },
  slots: {
    dualteaser: {
      type: 'dual-teaser'
    }
  }
});