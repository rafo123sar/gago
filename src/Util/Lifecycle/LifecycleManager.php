<?php declare(strict_types=1);

namespace SpsTheme\Util\Lifecycle;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\ActivateContext;
use Shopware\Core\Framework\Plugin\Context\DeactivateContext;
use Shopware\Core\Framework\Plugin\Context\InstallContext;
use Shopware\Core\Framework\Plugin\Context\UninstallContext;

class LifecycleManager
{

    private $plugin;
    private $container;
    private $context;
    private $cms_slot_repository;
    private $cms_block_repository;

    public function __construct(Plugin $plugin, $container)
    {
        $this->plugin = $plugin;
        $this->container = $container;
        $this->context = Context::createDefaultContext();

        $this->cms_slot_repository = $this->container->get('cms_slot.repository');
        $this->cms_block_repository = $this->container->get('cms_block.repository');
    }

    public function Install(InstallContext $context)
    {

    }

    public function Uninstall(UninstallContext $context)
    {
        if (!$context->keepUserData()) {
            /* Delete our CMS elements, since they will cause administration crash if left there */
            $this->DeleteCmsElement('quick-facts');
            /* Delete our CMS blocks, since they will cause administration crash if left there */
            $this->DeleteCmsBlock('quick-facts-block');
        }
    }

    private function DeleteCmsElement($id)
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('type', $id));
        $results = $this->cms_slot_repository->search($criteria, $this->context);
        if ($results != null) {
            $elements = $results->getEntities();
            foreach ($elements as $element) {
                $this->cms_slot_repository->delete([['id' => $element->getId()]], $this->context);
            }
        }
    }

    private function DeleteCmsBlock($id)
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('type', $id));
        $results = $this->cms_block_repository->search($criteria, $this->context);
        if ($results != null) {
            $blocks = $results->getEntities();
            foreach ($blocks as $block) {
                $this->cms_block_repository->delete([['id' => $block->getId()]], $this->context);
            }
        }
    }

    public function Activate(ActivateContext $context)
    {

    }

    public function Deactivate(DeactivateContext $context)
    {
    }
}
